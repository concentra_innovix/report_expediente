<?php

require_once('cone.php');
require "phpqrcode/qrlib.php";
$id = $_GET["id"];

//query DOCUMENT
$consulta = "SELECT Expediente.EXPEDNUMERO as NumeroExpediente,Expediente.EXPEDNUM,Expediente.OFICIONUMERO, Expediente.IESNOMBRE as ies, Expediente.IESSIGLAS,
Expediente.CARRRERANOMBRE as Carrera, Expediente.FECHAEXPEDIENTE as Fecha, CONCAT(Expediente.APELLIDO1, ' ',Expediente.APELIDO2) as Apellidos, Expediente.EXPED as Nombres,
Expediente.NUMEROIDENTIDAD as NumIdent, Expediente.EXPEDMATRICULA as Matricula
FROM DYNMESCYTHE05 Expediente where Expediente.EXPEDNUM = '$id'";
$ejecutar = sqlsrv_query($conn, $consulta);
$fila = sqlsrv_fetch_array($ejecutar);

//print_r($fila);

//Data
$noExp = $fila['EXPEDNUM'];
$ies = $fila['ies'];
$Carrera = $fila['Carrera'];
$Fecha = $fila['Fecha']->format('d/m/Y');
$Apellidos = $fila['Apellidos'];
$Nombres = $fila['Nombres'];
$Matricula = $fila['Matricula'];
$NumIdent = $fila['NumIdent'];
$siglas = $fila['IESSIGLAS'];
$oficio = $fila['OFICIONUMERO'];
$attribute = $noExp.','.$oficio.','.$siglas.','.$ies.','.$NumIdent.','.$Nombres.' '.$Apellidos.','.$Matricula.','.$Carrera;
$url = qr($attribute);

// new query
$query = "Select * from
(
select 'Record de Calificaciones' 'Documento',RCCOUNT as Cantidad
from  DYNMESCYTHE05
where EXPEDNUM = '".$noExp."'
UNION ALL
SELECT 'Copia de Título' 'Documento' ,CTCOUNT as Cantidad
from  DYNMESCYTHE05
where EXPEDNUM = '".$noExp."'
UNION ALL
select 'Carta de Grado' 'Documento', CGCOUNT as Cantidad
from  DYNMESCYTHE05
where EXPEDNUM = '".$noExp."'
UNION ALL
select 'Títiulo Original' 'Documento',TOCOUNT as Cantidad
from  DYNMESCYTHE05
where EXPEDNUM = '".$noExp."'
UNION ALL
select 'Pensum' 'Documento',PCOUNT as Cantidad
from  DYNMESCYTHE05
where EXPEDNUM = '".$noExp."'
UNION ALL
select 'Certificación de Escala de Calificaciones' 'Documento',CECALCOUNT as Cantidad
from  DYNMESCYTHE05
where EXPEDNUM = '".$noExp."'
UNION ALL
select 'Certificación de Indice' 'Documento',CICOUNT as Cantidad
from  DYNMESCYTHE05
where EXPEDNUM = '".$noExp."'
UNION ALL
select 'Certificación de Finalización Estudios' 'Documento',CFECOUNT as Cantidad
from  DYNMESCYTHE05
where EXPEDNUM = '".$noExp."'
UNION ALL
select 'Certificación de ultima Materia' 'Documento',CUMCOUNT as Cantidad
from  DYNMESCYTHE05
where EXPEDNUM = '".$noExp."'
UNION ALL
select 'Certificación de Postgrado' 'Documento', CPCOUNT as Cantidad
from  DYNMESCYTHE05
where EXPEDNUM = '".$noExp."'
UNION ALL
select 'Certificación de Revalida' 'Documento',CRCOUNT as Cantidad
from  DYNMESCYTHE05
where EXPEDNUM = '".$noExp."'
UNION ALL
select 'Certificación de Tesis de Grado y Postgrado' 'Documento',CTGPCOUNT as Cantidad
from  DYNMESCYTHE05
where EXPEDNUM = '".$noExp."'
UNION ALL
select 'Record de Transferencia y desglose de materias' 'Documento',RTDMCOUNT as Cantidad
from  DYNMESCYTHE05
where EXPEDNUM = '".$noExp."'
UNION ALL
select 'Certificación de Programas de Estudios de las Asignaturas' 'Documento',CPECOUNT as Cantidad
from  DYNMESCYTHE05
where EXPEDNUM = '".$noExp."'
UNION ALL
select 'Certificación de Calculo de Horas' 'Documento',CCHCOUNT as Cantidad
from  DYNMESCYTHE05
where EXPEDNUM = '".$noExp."'
UNION ALL
select 'Certificación de Equivalencia' 'Documento',CECOUNT as Cantidad
from  DYNMESCYTHE05
where EXPEDNUM = '".$noExp."'
UNION ALL
select 'Cartas Cálculos de Horas' 'Documento',CCHMEDCOUNT as Cantidad
from  DYNMESCYTHE05
where EXPEDNUM = '".$noExp."' 
UNION ALL
select 'Carta de Internado Rotatorio' 'Documento',CIRCOUNT as Cantidad
from  DYNMESCYTHE05
where EXPEDNUM = '".$noExp."'
UNION ALL
select 'Cartas Prácticas Hospitalarias' 'Documento',CPHCOUNT as Cantidad
from  DYNMESCYTHE05
where EXPEDNUM = '".$noExp."'
UNION ALL
select 'Cartas de Rotaciones Clínicas' 'Documento',CRCMED as Cantidad
from  DYNMESCYTHE05
where EXPEDNUM = '".$noExp."'
UNION ALL
select 'Cédula Anexa' 'Documento',CACOUNT as Cantidad
from  DYNMESCYTHE05
where EXPEDNUM = '".$noExp."'
UNION ALL
select 'Certificación de Bachiller Anexa' 'Documento',CBACOUNT as Cantidad
from  DYNMESCYTHE05
where EXPEDNUM = '".$noExp."'
UNION ALL
select 'Acta de Nacimiento Anexa' 'Documento',ANACOUNT as Cantidad
from  DYNMESCYTHE05
where EXPEDNUM = '".$noExp."'
UNION ALL
select 'Pasaporte (Extranjero) Anexo' 'Documento',PEACOUNT as Cantidad
from  DYNMESCYTHE05
where EXPEDNUM = '".$noExp."'
UNION ALL
select 'Record de Calificaciones Legalizado (Carrera base)' 'Documento',RCLCOUNT as Cantidad
from  DYNMESCYTHE05
where EXPEDNUM = '".$noExp."'
UNION ALL
select 'Copia de título Legalizado (Carrera base)' 'Documento',CTLCOUNT as Cantidad
from  DYNMESCYTHE05
where EXPEDNUM = '".$noExp."'


) as t
where Cantidad > 0";


//generar QR
function qr($attribute){
	//Declaramos una carpeta temporal para guardar la imagenes generadas
	$dir = 'temp/';
	
	//Si no existe la carpeta la creamos
	if (!file_exists($dir))
        mkdir($dir);
	
        //Declaramos la ruta y nombre del archivo a generar
	$filename = $dir.'test.png';
 
        //Parametros de Condiguración
	
	$tamaño = 4; //Tamaño de Pixel
	$level = 'H'; //Precisión Alta
	$framSize = 2; //Tamaño en blanco
	//$contenido = 'https://192.168.153.59/se/document/dc_view_document/api_view_document.php?cddocument="'.$code.'"&nmfile="'.$titulo.'".pdf';; //Texto
	
        //Enviamos los parametros a la Función para generar código QR 
    QRcode::png($attribute, $filename, $level, $tamaño, $framSize); 
    return $dir.basename($filename);
}



?>