<?php
require_once('bakend.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.0/css/mdb.min.css" rel="stylesheet">
    <title>EXPEDIENTE</title>

    <style>
    td{
      padding: 2px;
      
      
    }
    b{
      font-size: 14px;
    }
    .font{
        font-size: 20px;
    }
    



    
    
    </style>
</head>
<body>

  <div class="container" style="padding: 20px; "  >
        <div class="row" >
            <div class="col-md-12 " >
                <img width="500" style="margin-left: -70px" class="float-left"  src="logo1.png" alt=""/>
            </div>
        </div>
    
        <hr style="border-color:#0A5994; border-width: 30px; width: 900px; margin-left: -5px;">
        <h4 style="color: white; margin-top: -42px; margin-left: 400px;" class=" font-weight-bold">Carátula</h4>
    
        
        <div class="row">
            <div class="col-md-12"  style="padding-top: 10px; padding-bottom: 10px;">
     
                <!--Table-->
                <table   border="1" width="880" >
                    <tbody>
                        <tr>
                                <td style="width: 220px" class="font-weight-bold"><b class="font" >No.EXPEDIENTE:</b></td>
                                <td style="width: 360px"><b class="font"><?php echo $noExp?></b></td>
                                <td style="width: 10px;   border-top: 1px solid white; border-bottom: 1px solid white;"></td>
                                <td class="font-weight-bold" ><bclass="font" >FECHA:</b></td>
                                <td ><b class="font"><?php echo $Fecha?></b ></td>
                        </tr>  
                    </tbody>
                </table>

                <h6 class=" font-weight-bold font" style="margin-top: 10px;"> Datos del solicitante:</h6>
                <table border="2" width="880" >
                    <tbody>
                        <tr> 
                            <td style="width: 220px" class="font-weight-bold"><b class="font">APELLIDOS:</b></td>
                            <td   ><b class="font"><?php echo utf8_encode($Apellidos)?></b></td>
                            <td class="font-weight-bold"><b class="font">NOMBRE:</b></td>
                            <td  ><b class="font"><?php echo utf8_encode($Nombres)?></b></td>               
                        </tr>
                        <tr>
                            <td class="font-weight-bold"><b class="font">CEDULA/PASAPORTE:</b></td>
                            <td COLSPAN="3" ><b class="font"><?php echo $NumIdent?></b></td>
                        </tr> 
                    </tbody>
                </table>

                <h6 class=" font-weight-bold font" style="margin-top: 20px;"> Datos  academicos:</h6>

                <table border="2" width="880" >
                    <!--Table body-->
                    <tbody>
                        <tr>
                            <td class="font-weight-bold"><b class="font">MATRICULA:</b></td>
                            <td><b class="font"><?php echo $Matricula?></b></td>
                            <td class="font-weight-bold"><b class="font">NO.OFICIO:</b></td>
                            <td><b class="font"><?php echo $oficio?></b></td>  
                        </tr>
                        
                        
                        <tr>
                            <td class="font-weight-bold"><b class="font">SIGLAS:</b></td>
                            <td   ><b class="font"><?php echo utf8_encode($siglas)?></b></td>
                            <td class="font-weight-bold"><b class="font">CARRERA:</b></td>
                            <td><b class="font"><?php echo utf8_encode($Carrera)?></b></td> 
                        </tr>
                        <tr> 
                            <td class="font-weight-bold"><b class="font">IES:</b></td>
                            <td  COLSPAN="3" ><b class="font"><?php echo utf8_encode($ies)?></b></td> 
                        </tr>
                        
                    </tbody>
                </table>
   
            </div>
            <hr style="border-color:#0A5994; border-width: 10px; width: 900px; margin-left: 10px;">
            <div class="col-md-3"></div>
            <div class="col-md-4">
                <img alt="testing"  class="rounded mx-auto d-block"  width="270"  src="barcode.php?text=MescytRC&size=40&orientation=horizontal&codetype=code128&print=false&sizeFactor=1"/>
                
                <img alt="testing2"  class="rounded mx-auto d-block" style="margin-top: 20px;"  width="250" src="<?php echo $url?>"/>    
            </div>
            <div class="col-md-5"></div>
        
            <div class="col-md-12">
                <h6 class=" font-weight-bold font" > Documentos Solicitados:</h6>

                <table border="2" width="880" >
                    <tbody>
                       
                                <tr>
                                    <td class="font-weight-bold font"><b>Documento</b></td>
                                    <td style="width: 20px;"class="font-weight-bold font"><b>Cantidad</b></td>
                                </tr>
                                <?php
                                $c = 0;
                            $ejecutar2 = sqlsrv_query($conn, $query);
                            while($fila = sqlsrv_fetch_array($ejecutar2,  SQLSRV_FETCH_ASSOC)){
                               
                                $c = $c + $fila['Cantidad'];
                                
                                 ?>
                                <tr>
                                    <td  ><b ><?php echo $fila['Documento']?></b></td>
                                    <td ><b ><?php echo $fila['Cantidad']?></b></td>
                                </tr>
                               
                                <?php
                            }
                                 ?>  
                                 <tr>
                                    <td style="background-color: #D4D2D2" class=" font-weight-bold font"><b >Total de Documentos</b></td>
                                    <td style="background-color: #D4D2D2"  class=" font-weight-bold font"><b ><?php echo $c?></b></td>
                                </tr>    
                             
                            
                    </tbody>
                </table>

            </div>
        </div>
        <hr style="border-color:#0A5994; border-width: 10px; width: 900px; margin-left: -5px;">
    
        
    
    </div>
  


    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/umd/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.0/js/mdb.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js" integrity="sha384-CchuzHs077vGtfhGYl9Qtc7Vx64rXBXdIAZIPbItbNyWIRTdG0oYAqki3Ry13Yzu" crossorigin="anonymous"></script>



    <script>

    window.print();

    </script>

</body>
</html>